import sidebar from './sidebar/en'
import btn from './btn/en'
import barProfile from './barProfile/en'
import textfield from './textfield/en'
import alert from './alert/en'
import loading from './loading/en'

export default {
  sidebar,
  btn,
  barProfile,
  textfield,
  alert,
  loading,
  langId: 'en',
  langName: 'English',
  languages: {
    thai: 'ไทย',
    english: 'English',
    china: '中文'
  }
}
