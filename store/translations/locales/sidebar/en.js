const sidebar = {
  textDashboard: 'Dashboard',
  textUserManagement: 'User Management',
  textTransactionReport: 'Transaction Report',
  textSystemLog: 'System Log',
  textUserManagementAddAgent: 'add agent',
  textUserManagementDownlineList: 'Downline list',
  textTransactionReportWinLose: 'Win Lose',
  textTransactionReportWinLoseDetails: 'Win Lose Details',
  textTransactionReportWinLoseKiosk: 'Win Lose (Kiosk)',
  textTransactionReportOnlineUsers: 'Online Users',
  textTransactionReportPrizeTransactions: 'Prize Transactions',
  textSystemLogCreditlog: 'Credit log',
  textSystemLogViewLog: 'View Log'
}

export default sidebar
