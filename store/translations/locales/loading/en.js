const loading = {
  textPreloadTitle: 'Processing...',
  textPreloadSubTitle: 'Please wait'
}

export default loading
