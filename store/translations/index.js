import th from './locales/th'
import en from './locales/en'
import cn from './locales/cn'

export default {
  th,
  en,
  cn
}
