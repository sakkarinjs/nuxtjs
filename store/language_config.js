import languages from './translations'

const supportedLanguages = Object.getOwnPropertyNames(languages)

export const state = () => ({
  language: ''
})

export const getters = {
}

export const mutations = {
  setLanguage (
    state,
    payload
  ) {
    state.language = payload
  }
}

export const actions = {
  postDataStoreLanguageType (
    {
      commit,
      state
    },
    languages
  ) {
    if (
      typeof languages === 'string'
    ) {
      commit('setLanguage', languages)
    } else {
      const language = supportedLanguages.find(sl =>
        languages.find(l => (l.split(new RegExp(sl, 'gi')).length - 1 > 0 ? sl : null)))
      commit('setLanguage', language)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
