export default {
  dataUsers (state) {
    return state.dataUsers
  },
  itemsPage (state) {
    return state.itemsPage
  },
  headers (state) {
    return state.headers
  }
}
