// import colors from 'vuetify/es5/util/colors'
import msgTranslations from './store/translations'

export default {
  ssr: false,
  target: 'static',
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - nuxtjs',
    title: 'nuxtjs',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: ''
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/logo.png'
      }
    ]
  },
  loading: {
    color: '#f47d23',
    height: '2px'
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/sass/mianstyle.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vuetify.js',
    '~/plugins/snotify.js',
    '~/plugins/vue-currency-filter.js',
    '~/plugins/vuelidate.js',
    '~/plugins/vue-meta.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/moment'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    [
      'nuxt-vuex-localstorage', {
        mode: 'debug',
        localStorage: [
          'theme_config',
          'language_config'
        ]
      }
    ],
    '@nuxtjs/pwa',
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: 'AIzaSyB3oLVBBpuVFFqoNOHm351nVLi4CWsawt0',
          authDomain: 'shop-management-3313a.firebaseapp.com',
          projectId: 'shop-management-3313a',
          storageBucket: 'shop-management-3313a.appspot.com',
          messagingSenderId: '808170133478',
          appId: '1:808170133478:web:7f7587d60886fe209d6827',
          measurementId: 'G-T8V4BSNWHQ'
        },
        services: {
          auth: {
            persistence: 'local',
            initialize: {
              onAuthStateChangedAction: 'onAuthStateChangedAction',
              subscribeManually: false
            },
            ssr: false
          },
          firestore: {
            enablePersistence: true
          },
          storage: true
        }
      }
    ],
    'nuxt-i18n'
  ],
  i18n: {
    locales: [
      'th',
      'en',
      'cn'
    ],
    defaultLocale: 'th',
    vueI18n: {
      fallbackLocale: 'th',
      messages: msgTranslations
    }
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // baseURL: 'https://api.palin-2559.com/webservices/'
  },
  router: {
    middleware: [
      'auth-permission'
    ]
  },
  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    theme: {
      dark: false,
      themes: {
        // dark: {
        //   primary: colors.blue.darken2,
        //   accent: colors.grey.darken3,
        //   secondary: colors.amber.darken3,
        //   info: colors.teal.lighten1,
        //   warning: colors.amber.base,
        //   error: colors.deepOrange.accent4,
        //   success: colors.green.accent3
        // }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      'nuxt-vuex-localstorage'
    ]
  }
}
